#include <Constants.au3>
#include <EditConstants.au3>
#include <File.au3>
#include <FileConstants.au3>
#include <GUIConstantsEx.au3>
#include <GuiEdit.au3>
#include <MsgBoxConstants.au3>
#include <ScrollBarsConstants.au3>
#include <WinAPIFiles.au3>
#include <WindowsConstants.au3>
#include <Date.au3>
#include <DateTimeConstants.au3>


HotKeySet("{F10}", "druidSoloTag")
HotKeySet("{F11}", "easterEggs2")
HotKeySet("{F12}", "newWorldFishing")
HotKeySet("{F9}", "waiting")
HotKeySet("{F8}", "leftClick")

HotKeySet("{F2}", "stopProgram")
HotKeySet("{F1}", "stopProgram")

HotKeySet("{F7}", "gtaDanceNightclub")

_Main()



func newWorldFishing()
   writeLog("Starting New World Fishing")
   Global $loopCounter = 0
   Global $outerLoop = 0
   ;Global $greenWheelColor = 0x259B78 ;green wheel that indicates fishing is coming
   Global $greenWheelColor = 0x40B18C ;green wheel that indicates fishing is coming

   Global $yellowWheelColor = 0xEBC94E ;yellow wheel that indicates fishing is coming
   Global $searchLeft = 896
   Global $searchTop = 25
   Global $searchRight = 1805
   Global $searchBottom = 1048
   Global $currentFishTimer = TimerInit()
   while $outerLoop < 50
	  $loopCounter = 0
	  Global $intermittenTimer = TimerInit()
	  while $loopCounter < 25000
		  ;LOOK TO SEE IF YOU"RE IN THE GAME
		  ;$inGame = PixelSearch(122,50,125,53, $greenWheelColor, 5) ; Look for user selected color
		  ;If @error = 1 Then
			  ;ToolTip("NOT IN GAME, RESTARTING", 0, 0) ; displays tooltip in top left corner
			  ;Sleep(50000)
			  ;restartGame()
		  ;EndIf




		  ToolTip("Casting...", 0, 0) ; displays tooltip in top left corner
		  ToolTip("LoopCounter: " & $loopCounter, 0, 20) ; displays tooltip in top left corner
		  writeLog("LoopCounter: " & $loopCounter)
		  Sleep(1000)

		  MouseDown("Left")
		  sleep(2000); THIS IS MAX THROW
		  ;sleep(500)
		  ;sleep(50)
		  MouseUp("Left")

		  Sleep (2000)
		  $currentFishTimer = TimerInit() ; Set a timeout for finding a catch

		  ToolTip("looking for the fish...", 0, 0)
		 while Round(timerdiff($currentFishTimer)/1000,0) <= 30

		 ToolTip("performing next search", 0, 0)

		  ; looking for green color
		  $bobber = PixelSearch($searchLeft,$searchTop,$searchRight,$searchBottom, $greenWheelColor, 2)
			 If @error = 1 Then
				 ToolTip("Have not caught fish yet, about to continue looping", 0, 0)
			  Else
				 ToolTip("WE FOUND THE FISH, EXITING..", 0, 0)
				 ExitLoop
			  EndIf

		   ;looking for yellow color
		  $bobber = PixelSearch($searchLeft,$searchTop,$searchRight,$searchBottom, $yellowWheelColor, 2)
			 If @error = 1 Then
				 ToolTip("Have not caught fish yet, about to continue looping", 0, 0)
			  Else
				 ToolTip("WE FOUND THE FISH, EXITING..", 0, 0)
				 ExitLoop
			  EndIf
		 WEnd

		 ToolTip("WE FOUND A FISH", 0, 0)
		 newWorldReelFishIn()
		 newWorldFishCheckIfCaughtSpecialFish()
		 sleep(300)
		 newWorldReelFishIn()
		 newWorldFishCheckIfCaughtSpecialFish()
		 $counter = logCounter($loopCounter)

		 ToolTip("fish round complete, waiting to start next round", 0, 0)

		 send("{F3}")
		 sleep(2000)

		 if Round(timerdiff($intermittenTimer)/1000,0) >= 300 Then;If it's been 5 minutes (300 seconds) then we'll try to do something like lures/whatever
			$intermittenTimer = TimerInit() ; reset timer
			writeLog("It's been 5 minutes, moving slightly to make sure we're not afk")
			send("s")
			sleep(500)
			send("w")
			sleep(500)
		 EndIf


		 send("{F3}")
		 sleep(2500)
		  $loopCounter = $loopCounter + 1


	   WEnd
	  $outerLoop = $outerLoop + 1
   WEnd
EndFunc

func newWorldFishCheckIfCaughtSpecialFish()
	; looking for green color
	Local $newWorldGreenitemPickupColor = 0x46F76D
	$bobber = PixelSearch($searchLeft,$searchTop,$searchRight,$searchBottom, $newWorldGreenitemPickupColor, 2)
	   If @error = 1 Then
		   ToolTip("We did not catch a special item", 0, 0)
		   writeLog("No special item")
		   Return
		Else
		   ToolTip("We Caught a special item..", 0, 0)
		   writeLog("Found a special green tier item")
		   sleep(5000)
		   _MouseMovePlus(600,1000)
		   sleep(500)
		   _MouseMovePlus(250,600)
		   ;sleep(1000000)
		EndIf
EndFunc

Func _Main()
	Local $idFishMenu, $idFishStart, $idFishStop, $idDruidTag, $idDruidTagStop
	Local $idOkButton, $idClickMenu, $idClickStart, $idClickStop, $idClickStartWithTimer
	Local $idGameMenu, $idGTA
	Local $iMsg, $sFile
	Global $logPath = @ScriptDir & "\logs\" & @MON & @MDAY & @HOUR & @MIN  & ".log"
    Global $fileReadPrev
	Global $lootCount = 1

	#forceref $idSeparator1

	GUICreate("Notepad", 450, 800)

	$idClickMenu = GUICtrlCreateMenu("Clicker") ; Click menu
	$idClickStart = GUICtrlCreateMenuItem("Start (F8)", $idClickMenu)
	$idClickStartAtTime = GUICtrlCreateMenuItem("Start At Specific Time", $idClickMenu)
	$idClickStartWithTimer = GUICtrlCreateMenuItem("Start With Timer (F7)", $idClickMenu)

	$idClickStop = GUICtrlCreateMenuItem("Stop (F9)", $idClickMenu)



	$idGameMenu = GUICtrlCreateMenu("Games") ; Games menu
	$idNewWorldFishing = GUICtrlCreateMenuItem("New World Fishing - Start" , $idGameMenu)
	$idNewWorldRunCollectingRandom = GUICtrlCreateMenuItem("New World Run - Collecting" , $idGameMenu)
	$idNewWorldSitStillCollecting = GUICtrlCreateMenuItem("New World Sit Still Collect (E)" , $idGameMenu)


	$idGTAGameMenu = GUICtrlCreateMenu("GTA", $idGameMenu)
	$idGTAMissionRepeat = GUICtrlCreateMenuItem("GTA Mission Repeat", $idGTAGameMenu)
	$idBlueStackHealSov = GUICtrlCreateMenuItem("State Of Survival - Heal", $idGTAGameMenu)



	$idWoWMenu = GUICtrlCreateMenu("World of Warcraft", $idGameMenu) ; World Of Warcraft

	$idFishStart = GUICtrlCreateMenuItem("Fish Start", $idWoWMenu)
	$idFishStop = GUICtrlCreateMenuItem("Fish Stop", $idWoWMenu)

	$idDruidStop = GUICtrlCreateMenuItem("Druid Stop", $idWoWMenu)

	$idDruidTagStop = GUICtrlCreateMenuItem("Druid Tag Stop", $idWoWMenu)

	Global $idClearButton = GUICtrlCreateButton("Clear Logs", 300, 700, 70, 20)



	GUISetState()

    $hLogTemp = FileRead($logPath)
    Global $logOutputBox = GUICtrlCreateEdit($hLogTemp, 15, 25, 400, 650, $WS_VSCROLL)
	FileClose($hLogTemp)

	writeLog("Application Starting")

	While 1
		$iMsg = GUIGetMsg()
	    updateLog()
		Select
		Case $iMsg = $GUI_EVENT_CLOSE
		   ExitLoop
	    Case $iMsg = $idClearButton
		   writeLog("Button Clicked")
		Case $iMsg = $idClickStart
		   leftClick()
		Case $iMsg = $idNewWorldFishing
		   newWorldFishing()
		Case $iMsg = $idNewWorldRunCollectingRandom
		   newWorldRunCollecting()
	    Case $iMsg = $idNewWorldSitStillCollecting
		   newWorldSitStillCollect()
		Case $iMsg = $idClickStartAtTime
		   leftClickAtTime()
		Case $iMsg = $idClickStartWithTimer
		   leftClickWithTimer()
		Case $iMsg = $idClickStop
		   waiting()
		Case $iMsg = $idGTAMissionRepeat
		   gtaMissionRepeat()
		 Case $iMsg = $idBlueStackHealSov
		   gameBlueStackHeal()
		EndSelect
	    Sleep(50)
	WEnd

	GUIDelete()

	Exit
 EndFunc   ;==>_Main

func newWorldSitStillCollect()
   writeLog("New World Sit Still Collecting - STARTING")
   sleep(1000)
   Global $newWorldRelogNeededTimer = TimerInit()
   Global $newWorldSitStillCollectTimer = TimerInit()
   while Round(timerdiff($newWorldSitStillCollectTimer)/1000,0) <= 18000 ;run for a total of 5 hours
	  if Round(timerdiff($newWorldRelogNeededTimer)/1000,0) >= 1800 Then ;relog every 30 minutes
		 writeLog("New World Sit Still Collecting - RESTARTING")
		 $newWorldRelogNeededTimer = TimerInit()
		 send("{ESC}")
		 sleep(3000)
		 MouseClick("left", 2154, 50)
		 sleep(3000)
		 MouseClick("left", 1355, 680)
		 sleep(3000)
		 MouseClick("left", 1354, 854)
		 sleep(25000)

		 MouseClick("left", 2206, 1288)
		 sleep(5000)
		 MouseClick("left", 2206, 1288)
		 sleep(30000)
		 writeLog("New World Sit Still Collecting - Restart Complete")
	  EndIf

	  send("e")
	  sleep(3000)
   WEnd
   writeLog("New World Sit Still Collecting - Script complete!")
EndFunc

func newWorldRunCollecting()
   writeLog("New World Run Collecting - Starting")
   sleep(1000)
   Global $newWorldRunCollectingTimer = TimerInit()
   Global $newWorldRunCollectingJumpingTImer = TimerInit()
   while Round(timerdiff($newWorldRunCollectingTimer)/1000,0) <= 1200

	  if Round(timerdiff($newWorldRunCollectingJumpingTImer)/1000,0) >= 120 Then
		 $newWorldRunCollectingJumpingTImer = TimerInit()
		 MouseClick("middle")
	     sleep(400)
	     send("{SPACE}")
		 sleep(100)
	  Else
		 send("e")
		 sleep(100)
		 MouseClick("middle")
		 sleep(400)
	  EndIf

	  send("e")
	  sleep(100)
	  send("e")
	  sleep(100)
	  send("e")
	  sleep(100)
	  send("e")
	  sleep(100)
	  send("e")
	  sleep(100)
	  send("e")
	  sleep(100)
	  send("e")
	  sleep(100)
	  send("e")
	  sleep(100)
	  send("e")
	  sleep(100)
	  send("e")
	  sleep(100)
	  send("e")
	  sleep(100)
	  send("e")
	  sleep(100)
	  send("e")
	  sleep(100)
	  send("e")
	  sleep(100)
	  send("e")
	  sleep(100)
	  send("e")
	  sleep(100)
	  send("e")
	  sleep(100)
	  send("e")
	  sleep(100)
	  send("e")
	  sleep(100)
	  send("e")
	  sleep(100)
	  send("e")
	  sleep(100)
	  send("e")
	  sleep(100)
	  send("e")
   WEnd
   sleep(100)
   send("w")
   writeLog("New World Run Collecting - FINISHED")

EndFunc

func newWorldReelFishIn()
	  ToolTip("starting to reel the fish in", 0, 0)
	   Local $newWorldReelFishTimer = timerinit()
	   while Round(timerdiff($newWorldReelFishTimer)/1000,0) <= 60

		  $bobber = PixelSearch($searchLeft,$searchTop,$searchRight,$searchBottom, $greenWheelColor, 20)

		  ;If @error = 0 Then ;error outputting
			;ToolTip("We found the fish wheel at " & $bobber[0] & ","& $bobber[1], 0, 0)
			;writeLog("We found the fish wheel at " & $bobber[0] & ","& $bobber[1])
		 ;EndIf

			If @error = 1 Then
			 ;we're going to search twice.. If we didn't find the green one, then we loook for yellow.. If we still didn't find yellow then we're done
			 sleep(50)
			 @error = 0
			 $yellowwheelSearch = PixelSearch($searchLeft,$searchTop,$searchRight,$searchBottom, $yellowWheelColor, 20)
			 If @error = 1 Then
			     ToolTip("We got the fish in the bag, continuing", 0, 0)
				 writeLog("We got the fish in the bag, moving to next round")
				 MouseClick("left") ;exits the fishing animation
				 ExitLoop

			  Else
			   ContinueLoop
			  EndIf
		   Else
			   MouseDown("left")
			   Sleep(1000)
			   MouseUp("left")
			   Sleep(700)
		   EndIf
	   WEnd
EndFunc


func stopProgram()
   while 1
	  sleep(100000)
   WEnd
EndFunc

func gameBlueStackHeal()
   writeLog("Starting bluestack heal")
   while 1
	  MouseClick("left", 886, 314, 1, 1)
	  sleep(1000)
	  MouseClick("left", 1426, 706, 1, 1)
	  sleep(1000)
	  MouseClick("left", 1010, 448, 1, 1)
	  sleep(1000)
	  send("2500")
	  send("{ENTER}")
	  sleep(1000)
	  MouseClick("left", 1412, 899, 1, 1)
	  sleep(2000)

	  MouseClick("left", 996, 499, 1, 1) ;clicking hospital roof
	  sleep(1000)
	  MouseClick("left", 903, 270, 1, 1) ;clicking share

	  sleep(30000)
	  writeLog("healing done")
   WEnd
EndFunc

func gtaMissionRepeat()
   writeLog("Starting GTA mission repeat")
   local $counter = 1
   local $numNotInGame = 0
   local $isSocialClubOpen = False
   while 1
	  $numNotInGame = inGTAMission($numNotInGame)

	  if $numNotInGame > 9 Then
		 WinActivate("GTA5.exe")
		 Sleep(200)
	     send("{PGUP}")
	     Sleep(500)
		 if gtaIsSocialClubOpen() Then
			writeLog("GTA Social Club is open, closing it now")
			send("{ESC}")
			Sleep(1000)
		 EndIf
		 send("{PGUP}")
	  EndIf

	  if $numNotInGame = 10 Then
		writeLog("WARNING! Haven't been in GTA mission this many times: " & $numNotInGame)
	 EndIf

	  if $numNotInGame > 35 Then
	    gtaExitRestart()
		gtaDanceNightclub()
		gtaEnterMission()
	  ElseIf $numNotInGame = 21 Then
		writeLog("WARNING! Possibly in mission select, selecting replay if possible")
		gtaMissionSelectReplay()
	  EndIf

	  sleep(1000)



      $counter = logCounter($counter)
	WEnd
 EndFunc


func gtaIsSocialClubOpen()
   local $pixelLoc1 = PixelGetColor(1713,61)
   local $pixelLoc2 = PixelGetColor(335, 61)
   if $pixelLoc1 = 0xFFA819 and $pixelLoc1 = 0xFFA819 then ;Enter button exists
	  return True
   Else
	  return False
   EndIf

   ;local $pixelLoc1 = PixelGetColor(1279,78)
   ;local $pixelLoc2 = PixelGetColor(1342,195)
   ;if $pixelLoc1 = 0xFFFFFF and $pixelLoc1 = 0xFFFFFF then ;Enter button exists
	  ;return True
   ;Else
	  ;return False
   ;EndIf
EndFunc


func gtaDanceNightclub()
   writeLog("Dancing in the GTA nightclub")
   sleep(1000)
   WinActivate("GTA5.exe")
   sleep(5000)
   MouseClick("right",600,600,1)
   sleep(2000)
   send("{w down}")
   sleep(1000)
   send("{w up}")
   sleep(3000)
   writeLog("about to start dancing")
   send("{e down}")
   sleep(4000)
   send("{e up}")
   sleep(5000)
   while 1
	  MouseClick("left")
	  sleep(500)
   WEnd
EndFunc

func gtaExitRestart()
   writeLog("ERROR! Restarting GTA")
   ;Close the game
   ProcessClose( "GTA5.exe" )
   sleep(10000)
   MouseClick("left", 640, 452)
   ;MouseClick("left", 635, 465)
   sleep(240000)
   writeLog("GTA game restart complete")
EndFunc

func gtaEnterMission()
   writeLog("Attempting to rejoin mission in GTA")
   local $gtaEnterMissionLoop = 0
   do
	  send("{ESC}")
	  sleep(2000)
	  if gtaIsEnterButtonAvailable() Then
		 ExitLoop
	  EndIf
	  $gtaEnterMissionLoop += 1
   until $gtaEnterMissionLoop = 10

   MouseClick("left", 629,201) ; click on online
   sleep(1000)
   MouseClick("left", 629,201) ; click on online
   sleep(1000)
   MouseClick("left", 629,201) ; click on online
   sleep(1000)
   WinActivate("GTA5.exe")

   MouseClick("left", 237, 252) ; click near job area just to draw focus
   sleep(2000)

   MouseClick("left", 375,261,1) ; click on Jobs
   WinActivate("GTA5.exe")
   sleep(2000)
   MouseClick("left", 375,261,1) ; click on Jobs
   sleep(3000)


   ;MouseClick("left", 532,265) ;select jobs
   ;sleep(2000)
   ;MouseClick("left", 420,300) ; double click on "play job"
   ;sleep(1000)
   ;MouseClick("left", 420,300) ; double click on "play job"

   ;send("{DOWN}")
   ;sleep(2000)
   ;send("{enter}")
   MouseClick("left", 393,299,1) ;select Play Job
   WinActivate("GTA5.exe")
   sleep(2000)
   MouseClick("left", 393,299,1) ;select Play Job
   sleep(3000) ; should be at "play job"
   writeLog("should be in 'play job'")


   MouseClick("left", 408,382,1) ;select Rockstar Created
   WinActivate("GTA5.exe")
   sleep(2000)
   MouseClick("left", 408,382,1) ;select Rockstar Created

   sleep(3000) ; should be at "Rockstar Created"
   writeLog("should be in 'Rockstar Created'")


   send("{down}")
   sleep(2000)
   send("{down}")
   sleep(2000)
   send("{down}")
   sleep(2000)
   send("{enter}")
   sleep(2000) ; enter rockstar created

   ;sleep(1000)
   ;MouseClick("left", 500,378) ; double click on "rockstar created"
   ;sleep(1000)
   ;MouseClick("left", 500,378) ; double click on "rockstar created"

   ;sleep(1000)
   ;MouseClick("left", 446,624) ; double click on "missions"
   ;sleep(1000)
   ;MouseClick("left", 446,624) ; double click on "missions"

   send("{down}")
   sleep(1000)
   send("{down}")
   sleep(1000)
   send("{down}")
   sleep(1000)
   send("{down}")
   sleep(1000)
   send("{down}")
   sleep(1000)
   send("{down}")
   sleep(1000)
   send("{down}")
   sleep(1000)
   send("{down}")
   sleep(1000)
   send("{down}")
   sleep(1000)
   send("{enter}")
   sleep(1000) ; enter 'missions selection'
   writeLog("should be in mission selection")


   writeLog("Mission select: about to find the correct job")
   sleep(1000)
   ;send down 27 times to select "denial of service job"
   send("{down}")
   sleep(300)
   send("{down}")
   sleep(300)
   send("{down}")
   sleep(300)
   send("{down}")
   sleep(300)
   send("{down}")
   sleep(300)
   send("{down}")
   sleep(300)
   send("{down}")
   sleep(300)
   send("{down}")
   sleep(300)
   send("{down}")
   sleep(300)
   send("{down}") ;10th click
   sleep(300)
   send("{down}")
   sleep(300)
   send("{down}")
   sleep(300)
   send("{down}")
   sleep(300)
   send("{down}")
   sleep(300)
   send("{down}")
   sleep(300)
   send("{down}")
   sleep(300)
   send("{down}")
   sleep(300)
   send("{down}")
   sleep(300)
   send("{down}")
   sleep(300)
   send("{down}") ;20th click
   sleep(300)
   send("{down}")
   sleep(300)
   send("{down}")
   sleep(300)
   send("{down}")
   sleep(300)
   send("{down}")
   sleep(300)
   send("{down}")
   sleep(300)
   send("{down}")
   sleep(300)
   send("{down}") ;27th click
   sleep(300)
   send("{ENTER}") ;enter
   sleep(1000)
   send("{ENTER}") ;enter

   sleep(10000) ; waiting for the "Alert, no active contact mission found, do you want to host"
   send("{ESC}") ;in host lobby now
   writeLog("Mission select: in host lobby now")
   sleep(5000)
   send("{RIGHT}") ;select difficulty
   sleep(500)
   send("{UP}") ;get to confirm setting
   sleep(500)
   send("{ENTER}")
   sleep(500)
   send("{UP}") ;get to play button
   sleep(500)
   send("{ENTER}")
   sleep(500)
   send("{ENTER}")
   Sleep(5000)
   writeLog("Back in GTA mission")
   sleep(500000)

EndFunc

func gtaIsEnterButtonAvailable()
   local $enterButtonLoc = PixelGetColor(1853,1038)
   local $enterButtonLoc2 = PixelGetColor(1870,1056)
   if $enterButtonLoc = 0xF4F4F4 and $enterButtonLoc2 = 0xF4F4F4 then ;Enter button exists
	  return True
   EndIf
EndFunc


func gtaMissionSelectReplay()
   send("{DOWN}")
   sleep(300)
   send("{DOWN}")
   sleep(300)
   send("{ENTER}")
   sleep(300)
   send("{ENTER}")
   sleep(300)
   send("{ENTER}")
   sleep(20000)
EndFunc

func inGTAMission($numNotInGame)
   local $gtaTeamLivesColor = PixelGetColor(1700,996)
   local $gtaStillInMissionCounter = 0
   if $gtaTeamLivesColor = 0xF0F0F0 Then ;team lives text is white and exists
	   $numNotInGame = 0
	Else
	   if $numNotInGame = 1 Then
		 writeLog("Not in game anymore")
	  EndIf
	  $numNotInGame += 1
    EndIf

	return $numNotInGame
EndFunc

func logCounter($counter)
   if $counter > 50000 Then
	   if Mod($counter, 50000) = 0 Then
		  writeLog("Looped this many times so far:" & $counter )
	   EndIf
	ElseIf $counter > 5000 Then
	   if Mod($counter, 5000) = 0 Then
		  writeLog("Looped this many times so far:" & $counter )
	   EndIf
    ElseIf $counter > 500 Then
	   if Mod($counter, 500) = 0 Then
		  writeLog("Looped this many times so far:" & $counter )
	   EndIf
    ElseIf $counter > 100 Then
	   if Mod($counter, 50) = 0 Then
		  writeLog("Looped this many times so far:" & $counter )
	   EndIf
    ElseIf $counter > 10 Then
	   if Mod($counter, 10) = 0 Then
		  writeLog("Looped this many times so far:" & $counter )
	   EndIf
    Else
	   writeLog("Looped this many times so far:" & $counter )
    EndIf
    $counter += 1
	Return $counter
EndFunc


func leftClickAtTime()

    GUICreate("My GUI get time", 200, 200, 800, 200)
    Local $idDate = GUICtrlCreateDate("", 20, 20, 100, 20, $DTS_TIMEFORMAT)
    GUISetState(@SW_SHOW)

    ; Loop until the user exits.
    While 1
        Switch GUIGetMsg()
            Case $GUI_EVENT_CLOSE
                ExitLoop

        EndSwitch
	 WEnd

    writeLog("Will start leftClick at a time of: " & GUICtrlRead($idDate))
    $style = "yyyy/MM/dd HH:mm:s" ;
    $DTM_SETFORMAT_ = 0x1032

    GUICtrlSendMsg($idDate, $DTM_SETFORMAT_, 0, $style)

    $sleeptimer= _DateDiff('s', _NowCalc(), GUICtrlRead($idDate))
    writeLog("Sleeping for this many seconds: " & $sleeptimer)
	$sleeptimer = $sleeptimer * 1000 ;conver from seconds to milliseconds, what sleep() needs
    GUIDelete()

	sleep($sleeptimer)
    writeLog("Done sleeping, will now start script to click")
	leftClick()

EndFunc

func leftClickWithTimer()
   local $doUntilValue = InputBox("Left Click With Timer", "How many minutes should I click for?", "120")
   writeLog("Starting leftClick with a timer of " & $doUntilValue & " minutes")
   local $counter = 0
   while $doUntilValue >= $counter
	  MouseClick("left")
	  $counter += 1
	  Sleep(60000)
	  writeLog("Left clicked this many times so far:" & $counter )
   WEnd

   writeLog("We finished clicking because of the timer" )
    Sleep(5000000)
EndFunc




func leftClick()
   writeLog("Starting leftClick, interval of 60 seconds")
   local $counter = 1
   while 1
	  MouseClick("left")
	  $counter += 1
	  Sleep(60000)
	  writeLog("Left clicked this many times so far:" & $counter )
 WEnd



EndFunc

func druidSoloTag()
   	writeLog("Starting druidSoloTag")
    send("{NUMPAD6}")
	local $counter = 1
	while 1
	   needHeal()
	   castStarFallAtFeet()
	   send("d")
	   send("d")
	   send("d")
	   selectUntaggedEnemy()
	   if Mod($counter, 200) = 0 Then
		  loot()
	   EndIf
	   $counter += 1
    WEnd
EndFunc

func easterEggs()
   	writeLog("Starting easterEggs")
    send("{NUMPAD9}")
	local $counter = 1
	while 1
	   send("{NUMPAD9}")
	   MouseClick("right", 1124, 615, 2,1)
	   Sleep(30)
	   MouseClick("right", 1108, 339, 2,1)
	   Sleep(30)
	   MouseClick("right", 744, 612, 2,1)
	   Sleep(30)
	   send("2")
	   Sleep(30)

	   if Mod($counter, 200) = 0 Then
		  writeLog("Tried this many times:" & $counter )
	   EndIf
    WEnd
EndFunc


func easterEggs2()
   	writeLog("Starting easterEggs")
    send("{NUMPAD9}")
	local $counter = 1
	while 1

	   MouseClick("right", 740, 504, 1,1)
	   Sleep(90)
	   MouseClick("right", 922, 596, 1,1)
	   Sleep(90)
	   MouseClick("right", 1000, 635, 1,1)
	   Sleep(90)
	   MouseClick("right", 1164, 523, 1,1)
	   Sleep(90)

	   if Mod($counter, 5) = 0 Then
	   send("2")
	   Sleep(300)
	   EndIf


	   if Mod($counter, 200) = 0 Then
		  writeLog("Tried this many times:" & $counter )
		  send("{NUMPAD9}")
		  Sleep(50)
		  send("2")
		  Sleep(50)
		  send("2")
		  Sleep(50)
		  send("2")
		  Sleep(50)
		  send("2")
		  Sleep(50)
		  send("2")
		  Sleep(50)
		  send("2")
		  Sleep(50)
		  send("2")
		  Sleep(50)
		  send("2")
		  Sleep(50)
		  send("2")
		  Sleep(50)
		  send("2")
		  Sleep(50)
		  send("2")
		  Sleep(50)
		  send("2")
		  Sleep(50)
		 send("2")
		  Sleep(50)
		  send("2")
		  Sleep(50)
		  send("2")
		  Sleep(50)
		  send("2")
		  Sleep(50)
		  send("2")
		  Sleep(50)
		  send("2")
		  Sleep(50)
	   Sleep(30)
	   EndIf
	$counter += 1
    WEnd
EndFunc

Func _MouseMovePlus($X = "", $Y = "")
        Local $MOUSEEVENTF_MOVE = 0x1
    DllCall("user32.dll", "none", "mouse_event", _
            "long",  $MOUSEEVENTF_MOVE, _
            "long",  $X, _
            "long",  $Y, _
            "long",  0, _
        "long",  0)
EndFunc

 func druidTagInCircle()
	writeLog("Starting DruidTagInCircle")
    send("{NUMPAD6}")
	local $counter = 1
	while 1
	   needHeal()
	   castStarFall()
	   send("d")
	   send("d")
	   send("d")
	   selectUntaggedEnemy()
	   if Mod($counter, 200) = 0 Then
		  loot()
	   EndIf
	   $counter += 1
    WEnd
 EndFunc

func castStarFallAtFeet()
   local $starfall = PixelGetColor(151,63)
   if $starfall = 3820978 Then ;has enough power
	   writeLog("Casting Starfall")
	   send("7")
	   sleep(50)
	   MouseClick("left", 955, 611,1,1) ;cast at feet
	   sleep(1000)
	   send("2")
	   sleep(300)
	   Return True
   Else
	  Return False
   EndIf
EndFunc

func castStarFall()
   local $starfall = PixelGetColor(151,63)
   if $starfall = 3820978 Then ;has enough power
	   writeLog("Casting Starfall")
	   send("7")
	   sleep(50)
	   MouseClick("left", 970, 165,1,1) ;cast as far away as possible
	   Return True
   Else
	  Return False
   EndIf
EndFunc

 Func druidFarm()
	send("{NUMPAD6}")
	local $counter = 1
	while 1
	   needHeal()
	   ;if $counter = 5 Then ;do a special move every 5 rounds? maybe some sort of heals/buff
		  ;send(6)
		  ;sleep(200)
		  ;MouseClick("left", 947, 445, 1,1)
		  ;sleep(1000)
	   ;EndIf
	   needHeal()
	   selectEnemy()
	   send("4") ; Celestial Alignment
	   send("5") ; Celestial Alignment
	   sleep(1000)
	   selectEnemy()
	   send("1") ; starsurge
	   sleep(1000)
	   selectEnemy()
	   send("3") ; lunar strike
	   sleep(1300)
	   selectEnemy()
	   send("2") ; Celestial Alignment
	   sleep(2000)
	   needHeal()
	   selectEnemy()
	   send("1") ; starsurge
	   sleep(1000)
	   $counter += 1
	   if Mod($counter, 20) = 0 Then
		  loot()
		  send("{F1}")
	   EndIf

	   if Mod($counter, 100) = 0 Then
		  writeLog("Loop Counter: " & $counter)
	   EndIf

	   if $counter = 10000 Then
		 Send ("!{F4}")
		 Sleep(1000)
		 WinClose("World of Warcraft")
		 Sleep(50000)
		 waiting()
	   EndIf
    WEnd
 EndFunc


func loot()
    writeLog("Looting count: " & $lootCount)
	MouseClick("right", 963, 516)
	sleep(100)
	MouseClick("right", 880, 538)
	sleep(100)
	MouseClick("right", 1040, 544)
	sleep(100)

    MouseClick("right", 951, 658)
	sleep(100)
	MouseClick("right", 880, 658)
	sleep(100)
    MouseClick("right", 1040, 658)
	sleep(100)
    $lootCount += 1
 EndFunc

func selectUntaggedEnemy()
   Send("{TAB}")
   if isEnemySelected() and enemyNotTagged() Then
	   send("5")
	   sleep(900)
    EndIf
 EndFunc

func selectUntaggedEnemySolo()
   Send("{TAB}")
   if isEnemySelected() and enemyNotTagged() Then
	   send("5")
	   sleep(800)
	   send("2")
	   sleep(900)

    EndIf
 EndFunc

func enemyNotTagged()
   local $enemyColor = PixelGetColor(261,75)
   if $enemyColor = 16774772 Then ; color of sunfire debuff
	  Return False
   Else
	  Return True
   EndIf
EndFunc

func isEnemySelected()
   local $enemyColor = PixelGetColor(261,33)
   if $enemyColor = 13107200 Then
	  Return True
   Else
	  Return False
   EndIf
EndFunc

func selectEnemy()
    local $enemy = PixelSearch(261,33, 266,37, 0xC80000, 10) ; Look for user selected color
	If @error = 1 Then
	   send("{TAB}")
	   newEnemy()
	   Return False
    EndIf
EndFunc

func newEnemy() ;if there's a new enemy selected, we might need a few special attacks
   send("6")
   sleep(1050)
EndFunc

 func needHeal()
	;LOOK TO SEE IF YOU NEED HEALS
    local $healthhigh = PixelSearch(146,49,152,53, 0x00BE00, 10) ; Look for green health at low percentage
	If @error = 1 Then
	   writeLog("Critical health, extra healing performed")
	   send("F")
	   sleep(500)
	   send("F")
	   sleep(500)
	   send("F")
	   sleep(500)
	   send("F")
	   sleep(500)
	   send("F")
	   sleep(500)
	   send("F")
	   sleep(500)
	   send("F")
	   sleep(500)
	   Return
	EndIf

	local $healthlow = PixelSearch(178,49,186,53, 0x00C300, 10) ; Look for green health at high percentage
	If @error = 1 Then
	   send("G")
	   sleep(1500)
    EndIf

 EndFunc

 Func testFishing()
	while 1
	   writeLog("test")
	   sleep(1000)
    WEnd
 EndFunc

func waiting()
    writeLog("got into stop")
	While 1
		$iMsg = GUIGetMsg()
		Select
		Case $iMsg = $GUI_EVENT_CLOSE
		   ExitLoop
	    Case $iMsg = $idClearButton
		   writeLog("Button Clicked")
		   MsgBox($MB_SYSTEMMODAL, "Click", "You clicked the button")
		EndSelect
	WEnd
	GUIDelete()
	Exit
EndFunc

 Func updateLog()
    ; Open the file for reading and store the handle to a variable.
    Local $hFileOpen = FileOpen($logPath, $FO_READ)
    If $hFileOpen = -1 Then
        MsgBox($MB_SYSTEMMODAL, "", "An error occurred when reading the log file.")
        Return False
    EndIf

    ; Read the contents of the file using the handle returned by FileOpen.
    Local $sFileRead = FileRead($hFileOpen)

    ;See if we need to update the text box
    If $sFileRead <> $fileReadPrev Then
	  GUICtrlSetData($logOutputBox, $sFileRead)
	  $fileReadPrev = $sFileRead

   _GUICtrlEdit_Scroll($logOutputBox, $SB_PAGEDOWN)
   _GUICtrlEdit_Scroll($logOutputBox, $SB_PAGEDOWN)
   _GUICtrlEdit_Scroll($logOutputBox, $SB_PAGEDOWN)
   EndIf



 EndFunc

 Func writeLog($textToWrite)
	Local $hLog = FileOpen($logPath, 1)
	_FileWriteLog($hLog, $textToWrite) ; Write to the logfile passing the filehandle returned by FileOpen.
	FileClose($hLog) ; Close the filehandle to release the file.
	updateLog()
 EndFunc

