
HotKeySet("{F1}", "activate")
HotKeySet("{F2}", "stop")
HotKeySet("{F3}", "splitbag")
HotKeySet("{F4}", "turnin")

Sleep(5000)

Global $bobber
Global $counter = 0
Global $inGame

While 1
   sleep(500000)

WEnd


func activate()
   Global $counter = 0
   $mouse = MouseGetPos()
    ;$searchLeft = $mouse[0] - 200
    ;$searchTop = $mouse[1] - 200
    ;$searchRight = $mouse[0] + 200
    ;$searchBottom = $mouse[1] + 200
    Sleep(1000)
   while(1)
   Cast()
   WEnd
   sleep(5000000)
EndFunc

func splitFish($xfishloc, $yfishloc, $xconfloc, $yconfloc, $xbagspot, $ybagspot)
   $splitFishDelay = 400
   Send("{SHIFTDOWN}")
   MouseClick("LEFT", $xfishloc,$yfishloc, 1,1)
   Send("{SHIFTUP}")
   sleep($splitFishDelay)
   MouseClick("LEFT", $xconfloc, $yconfloc,1,1)
   Sleep($splitFishDelay)
   MouseClick("LEFT", $xbagspot, $ybagspot, 1,1) ;bag1, row2, column 1
   sleep($splitFishDelay)
EndFunc

func turnin()
   while(1)
   MouseClick("RIGHT", 964, 468, 1,1) ; middle of screen
   sleep(500)
   MouseClick("LEFT", 204, 298, 1,1) ; left quest menu selection
   sleep(500)
   MouseClick("LEFT", 89, 597, 1,1) ; Continue or Finish quest selection
   sleep(500)
   MouseClick("LEFT", 89, 597, 1,1) ; Continue or Finish quest selection just in case it requires two
   sleep(500)
   WEnd
EndFunc

func splitbag()
   ToolTip("Separating fishes", 0, 0) ; displays tooltip in top left corner
   Sleep(1000)


   ;bag 1, column 1
   Call("splitFish", 1721,744, 1591, 678, 1721 ,795)
   Call("splitFish", 1772,744, 1644, 678, 1721 ,841)
   Call("splitFish", 1721,744, 1591, 678, 1721 ,895)


   ;bag 1, column 2
   Call("splitFish", 1772,744, 1644, 678, 1775 ,795)
   Call("splitFish", 1721,744, 1591, 678, 1775 ,841)
   Call("splitFish", 1772,744, 1644, 678, 1775 ,895)


   ;bag 1, column 3
   Call("splitFish", 1721,744, 1591, 678, 1826 ,795)
   Call("splitFish", 1772,744, 1644, 678, 1826 ,841)
   Call("splitFish", 1721,744, 1591, 678, 1826 ,895)


   ;bag 1, column 4
   Call("splitFish", 1772,744, 1644, 678, 1884 ,795)
   Call("splitFish", 1721,744, 1591, 678, 1884 ,841)
   Call("splitFish", 1772,744, 1644, 678, 1884 ,895)


   ;bag 2, column 1
   Call("splitFish", 1721 , 744 , 1591 , 678, 1721 , 467 )
   Call("splitFish", 1772 , 744 , 1644 , 678, 1721 , 517 )
   Call("splitFish", 1721 , 744 , 1591 , 678, 1721 , 566 )
   Call("splitFish", 1772 , 744 , 1644 , 678, 1721 , 619 )

   ;bag 2, column 2
   Call("splitFish", 1721 , 744 , 1591 , 678 , 1775 , 467 )
   Call("splitFish", 1772 , 744 , 1644 , 678 , 1775 , 517 )
   Call("splitFish", 1721 , 744 , 1591 , 678 , 1775 , 566 )
   Call("splitFish", 1772 , 744 , 1644 , 678 , 1775 , 619 )

   ;bag 2, column 3
   Call("splitFish", 1721 , 744 , 1591 , 678, 1825 , 467 )
   Call("splitFish", 1772 , 744 , 1644 , 678, 1825 , 517 )
   Call("splitFish", 1721 , 744 , 1591 , 678, 1825 , 566 )
   Call("splitFish", 1772 , 744 , 1644 , 678, 1825 , 619 )

   ;bag 2, column 4
   Call("splitFish", 1721 , 744 , 1591 , 678 , 1881 , 467 )
   Call("splitFish", 1772 , 744 , 1644 , 678 , 1881 , 517 )
   Call("splitFish", 1721 , 744 , 1591 , 678 , 1881 , 566 )
   Call("splitFish", 1772 , 744 , 1644 , 678 , 1881 , 619 )


   ;bag 3, column 1
   Call("splitFish", 1721 , 744 , 1591 , 678, 1721 , 188 )
   Call("splitFish", 1772 , 744 , 1644 , 678, 1721 , 242 )
   Call("splitFish", 1721 , 744 , 1591 , 678, 1721 , 293 )
   Call("splitFish", 1772 , 744 , 1644 , 678, 1721 , 343 )

   ;bag 3, column 2
   Call("splitFish", 1721 , 744 , 1591 , 678 , 1775 , 188 )
   Call("splitFish", 1772 , 744 , 1644 , 678 , 1775 , 242 )
   Call("splitFish", 1721 , 744 , 1591 , 678 , 1775 , 293 )
   Call("splitFish", 1772 , 744 , 1644 , 678 , 1775 , 343 )

   ;bag 3, column 3
   Call("splitFish", 1721 , 744 , 1591 , 678, 1825 , 188 )
   Call("splitFish", 1772 , 744 , 1644 , 678, 1825 , 242 )
   ;-----First two stacks of fish are over, time for position 3 and 4
   Call("splitFish", 1826 , 744 , 1701 , 678, 1825 , 293 )
   Call("splitFish", 1878 , 744 , 1751 , 678, 1825 , 343 )

   ;bag 4, column 4
   Call("splitFish", 1826 , 744 , 1701 , 678, 1879 , 188 )
   Call("splitFish", 1878 , 744 , 1751 , 678, 1879 , 242 )
   Call("splitFish", 1826 , 744 , 1701 , 678, 1879 , 293 )
   Call("splitFish", 1878 , 744 , 1751 , 678, 1879 , 343 )

   ;bag 5, column 1
   Call("splitFish", 1826 , 744 , 1701 , 678, 1477 , 767 )
   Call("splitFish", 1878 , 744 , 1751 , 678, 1477 , 819 )
   Call("splitFish", 1826 , 744 , 1701 , 678, 1477 , 870 )
   Call("splitFish", 1878 , 744 , 1751 , 678, 1477 , 923 )

   ;bag 5, column 2
   Call("splitFish", 1826 , 744 , 1701 , 678, 1531 , 767 )
   Call("splitFish", 1878 , 744 , 1751 , 678, 1531 , 819 )
   Call("splitFish", 1826 , 744 , 1701 , 678, 1531 , 870 )
   Call("splitFish", 1878 , 744 , 1751 , 678, 1531 , 923 )

   ;bag 5, column 3
   Call("splitFish", 1826 , 744 , 1701 , 678, 1584 , 767 )
   Call("splitFish", 1878 , 744 , 1751 , 678, 1584 , 819 )
   Call("splitFish", 1826 , 744 , 1701 , 678, 1584 , 870 )
   Call("splitFish", 1878 , 744 , 1751 , 678, 1584 , 923 )

   ;bag 5, column 4
   Call("splitFish", 1826 , 744 , 1701 , 678, 1637 , 767 )
   Call("splitFish", 1878 , 744 , 1751 , 678, 1637 , 819 )
   Call("splitFish", 1826 , 744 , 1701 , 678, 1637 , 870 )
   Call("splitFish", 1878 , 744 , 1751 , 678, 1637 , 923 )

   ;bag 6, column 1
   Call("splitFish", 1826 , 744 , 1701 , 678, 1478 , 494 )
   Call("splitFish", 1878 , 744 , 1751 , 678, 1478 , 542 )
   Call("splitFish", 1826 , 744 , 1701 , 678, 1478 , 592 )
   Call("splitFish", 1878 , 744 , 1751 , 678, 1478 , 647 )

   ;bag 6, column 2
   Call("splitFish", 1826 , 744 , 1701 , 678, 1532 , 494 )
   Call("splitFish", 1878 , 744 , 1751 , 678, 1532 , 542 )
   Call("splitFish", 1826 , 744 , 1701 , 678, 1532 , 592 )
   Call("splitFish", 1878 , 744 , 1751 , 678, 1532 , 647 )

   ;bag 6, column 3
   Call("splitFish", 1826 , 744 , 1701 , 678, 1583 , 494 )
   Call("splitFish", 1878 , 744 , 1751 , 678, 1583 , 542 )
   Call("splitFish", 1826 , 744 , 1701 , 678, 1583 , 592 )
   Call("splitFish", 1878 , 744 , 1751 , 678, 1583 , 647 )

   ;bag 6, column 4
   Call("splitFish", 1826 , 744 , 1701 , 678, 1635 , 494 )

EndFunc


func stop()
   while(1)
   sleep(500000)
   WEnd
EndFunc

func CastWrapper()
   Send ( "!{F4}")
   Sleep(10000)
   MouseClick("left", 769,934,1,1) ;clicking the Play button
   Sleep(15000)
   MouseClick("left",661,272,2,3) ;choosing server
   Sleep(10000)
   MouseClick("left",959,991,1,3) ;hitting Enter World
   sleep(15000)
   $counter = 1
   Cast()
EndFunc

func restartGame()

   Send ("!{F4}")
   Sleep(1000)
   WinClose("World of Warcraft")
   Sleep(1000)
   WinClose("Blizzard Battle.net")

   Sleep(5000)
   Send ("{LWIN}") ;Hit the windows button
   Sleep(1000)
   MouseClick("left",692,659,1,1) ; select the launcher
   Sleep(10000)
   MouseClick("left", 547,59,1,1) ; Click on Games within the app
   sleep(2000)
   MouseClick("left", 490,155,1,1) ; Selecting the game
   sleep(1000)


   MouseClick("left", 769,934,1,1) ;clicking the Play button
   sleep(1000)
   MouseClick("left", 748,975,1,1) ;clicking the Play button (second option more further down)
   sleep(1000)
   MouseClick("left", 748,1016,1,1) ;clicking the Play button (third option more further down)
   sleep(1000)
   MouseClick("left", 748,1026,1,1) ;clicking the Play button (fourth option more further down)
   Sleep(10000)
   MouseClick("left",735,900,2,1) ;Choosing US East
   Sleep(2000)
   MouseClick("left",661,272,2,3) ;choosing server
   Sleep(5000)
   MouseClick("left",959,991,1,3) ;hitting Enter World
   sleep(12000)
   Send("]") ;Zooming in
   Sleep(500)
   Send("]") ;Zooming in
   Sleep(500)
   Send("]") ;Zooming in
   Sleep(500)
   Send("]") ;Zooming in
   Sleep(500)
   Send("]") ;Zooming in
   Sleep(500)
   Send("]") ;Zooming in
   Sleep(500)
   Send("]") ;Zooming in
   Sleep(500)
   Send("]") ;Zooming in
   Sleep(500)
   Send("]") ;Zooming in
   Sleep(500)
   Send("]") ;Zooming in
   Sleep(500)
   Send("]") ;Zooming in
   Sleep(500)
   Send("]") ;Zooming in
   Sleep(500)
   Send("]") ;Zooming in
   Sleep(500)
   Send("]") ;Zooming in
   Sleep(500)
   Send("]") ;Zooming in
   Sleep(500)
   Send("]") ;Zooming in
   Sleep(500)
   Send("]") ;Zooming in
   Sleep(500)
EndFunc

func lunarcap()
   Global $lunarcap = 0
   while $lunarcap < 5
	  Send("2")
	  Sleep(16000)
	  Send("3")
	  Sleep(15000)
	  $lunarcap = $lunarcap + 1
   WEnd

EndFunc
;Cast Function
func Cast()
   Global $loopCounter = 0
   Global $outerLoop = 0
   Global $color = 0x472212
   Global $searchLeft = 523
   Global $searchTop = 319
   Global $searchRight = 1351
   Global $searchBottom = 650
   while $outerLoop < 50
	  $loopCounter = 0
	  Global $intermittenTimer = TimerInit()
	  while $loopCounter < 25000
		  ;LOOK TO SEE IF YOU"RE IN THE GAME
		  $inGame = PixelSearch(122,50,125,53, 0x00AA00, 5) ; Look for user selected color
		  If @error = 1 Then
			  ToolTip("NOT IN GAME, RESTARTING", 0, 0) ; displays tooltip in top left corner
			  ;Sleep(50000)
			  restartGame()
		  EndIf

		 if Round(timerdiff($intermittenTimer)/1000,0) >= 600 Then;If it's been 10 minutes (600 seconds) then we'll try to do something like lures/whatever
			$intermittenTimer = TimerInit() ; reset timer
			;lunarcap()
		 EndIf


		  ToolTip("Casting...", 0, 0) ; displays tooltip in top left corner
		  ToolTip("LoopCounter: " & $loopCounter, 0, 20) ; displays tooltip in top left corner
		  Sleep(1000)
		  ;MouseClick("Right")
		  Send("1")
		  ToolTip("Searching for bobber...", 0, 0) ; displays tooltip in top left corner
		  Sleep (2000)
		  Global $current = timerinit() ; Set a timeout for finding splash


		  $bobber = PixelSearch($searchLeft,$searchTop,$searchRight,$searchBottom, $color, 20) ; Look for user selected color
		  If @error = 1 Then
			  ToolTip("Could not locate bobber, recasting...", 0, 0) ; displays tooltip in top left corner
			  Sleep(2000)
			  ContinueLoop
		  EndIf
		  If TimerDiff($current) > 5000 Then ;If the bobber is not seen in 5 sec. Then cast again
			  ContinueLoop
		  EndIf
		  sleep (1000)
		  MouseMove($bobber[0], $bobber[1]) ; Move the mouse to the bobber
		  ToolTip("Found bobber, waiting for fish to be hooked...", 0, 0) ; displays tooltip in top left corner
		  Sleep (500)

		  Global $current = timerinit() ; Set a timeout for finding splash
		  while 1
			 $splash = PixelSearch($bobber[0]-7,$bobber[1]-7,$bobber[0]+7,$bobber[1]+7, $color, 25) ; Search a tiny 20x20 square for the bobber color
			 If @error = 1 Then ExitLoop ; When the color isn't found, the bobber has bobbed
			 Sleep(100)
			 if Round(timerdiff($current)/1000,0) >= 25 Then;If the splash is not seen in 25 sec. Then cast again
				 ExitLoop
			 EndIf
		  WEnd

		  ;the bobber color has changed (a fish is hooked), loot bobber and repeat.
		  ToolTip("Splash detected, looting fish...", 0, 0) ; displays tooltip in top left corner
		  ;Sleep(Random(75,175))
		  MouseClick("Right", $bobber[0], $bobber[1], 1, 1)
		  Sleep(500)
		  ToolTip("Recasting...", 0, 0) ; displays tooltip in top left corner

		  $loopCounter = $loopCounter + 1


	   WEnd
	  $outerLoop = $outerLoop + 1
   WEnd
EndFunc